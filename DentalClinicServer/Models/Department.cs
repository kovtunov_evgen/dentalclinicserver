﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace DentalClinicServer.Models
{
    public class Department
    {
        
        [Key]
        public long IdDepartment { get; set; }
        public string Name { get; set; }
      
        public virtual Hospital Hospital { get; set; }
        public virtual ICollection<Personel> Personels { get; set; }
        public virtual ICollection<Patient> Patients { get; set; }
    }
}
