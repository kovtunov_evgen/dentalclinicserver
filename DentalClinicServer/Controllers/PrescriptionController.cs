﻿using DentalClinicServer.DAL;
using DentalClinicServer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DentalClinicServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Prescription")]
    public class PrescriptionController : Controller
    {
        private readonly DentalClinicContext _context;

        public class MedicinePatient
        {
            string firstName { get; set; }
            string lastName { get; set; }
            string medicineName { get; set; }

             public MedicinePatient(Medicine medicine, Patient patient) {
                this.firstName = patient.FirstName;
                this.lastName = patient.LastName;
                this.medicineName = medicine.Name;
            }
        }

        public PrescriptionController(DentalClinicContext context)
        {
            _context = context;
        }

        // GET: api/Prescription
        [HttpGet]
        public IEnumerable<Prescription> GetPrescriptions()
        {
            return _context.Prescriptions; 
           /* var medicines = _context.Medicines;
            var patients = _context.Patients;
            var prescriptions = _context.Prescriptions;
            List<MedicinePatient> asd = new List<MedicinePatient>();
            Medicine med;
            Patient pat;
            foreach(var prescription in prescriptions)
            {
                foreach(var medicine in medicines)
                {
                    if (prescription.Medicine == medicine)
                    {
                        foreach (var patient in patients)
                        {
                            if (prescription.Patient == patient)
                            {
                                MedicinePatient personelDepartment = new MedicinePatient(medicine, patient);
                                asd.Add(personelDepartment);

                            }
                        }
                    }
                }
               
               

            }
            return asd;*/
        }

        // GET: api/Prescription/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPrescription([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var prescription = await _context.Prescriptions.SingleOrDefaultAsync(m => m.IdPrescription == id);

            if (prescription == null)
            {
                return NotFound();
            }

            return Ok(prescription);
        }

        // PUT: api/Prescription/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPrescription([FromRoute] long id, [FromBody] Prescription prescription)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != prescription.IdPrescription)
            {
                return BadRequest();
            }

            _context.Entry(prescription).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PrescriptionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Prescription
        [HttpPost]
        public async Task<IActionResult> PostPrescription([FromBody] Prescription prescription)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Prescriptions.Add(prescription);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPrescription", new { id = prescription.IdPrescription }, prescription);
        }

        // DELETE: api/Prescription/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePrescription([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var prescription = await _context.Prescriptions.SingleOrDefaultAsync(m => m.IdPrescription == id);
            if (prescription == null)
            {
                return NotFound();
            }

            _context.Prescriptions.Remove(prescription);
            await _context.SaveChangesAsync();

            return Ok(prescription);
        }

        private bool PrescriptionExists(long id)
        {
            return _context.Prescriptions.Any(e => e.IdPrescription == id);
        }
    }
}