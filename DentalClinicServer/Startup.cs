﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DentalClinicServer.DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;


using Microsoft.EntityFrameworkCore;
using DentalClinicServer.Models;
using Microsoft.AspNetCore.Mvc.Cors.Internal;

namespace DentalClinicServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DentalClinicContext>(options => options.UseSqlServer(Configuration["ConnectionString:DentalClinic"]));
            // services.AddMvc();
            services.AddCors();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
           

           app.UseCors(options => options.WithOrigins("http://localhost:4200").AllowAnyMethod());
            app.UseMvc();
        }
    }
}
