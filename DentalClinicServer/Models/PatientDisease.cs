﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DentalClinicServer.Models
{
    public class PatientDisease
    {
        [Key]
        public long IdPatientDisease { get; set; }
        public int Days { get; set; }

        public virtual Patient Patient { get; set; }
        public virtual Disease Disease { get; set; }
    }
}
