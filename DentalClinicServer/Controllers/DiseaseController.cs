﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalClinicServer.DAL;
using DentalClinicServer.Models;

namespace DentalClinicServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Disease")]
    public class DiseaseController : Controller
    {
        private readonly DentalClinicContext _context;

        public DiseaseController(DentalClinicContext context)
        {
            _context = context;
        }

        // GET: api/Disease
        [HttpGet]
        public IEnumerable<Disease> GetDiseases()
        {
            return _context.Diseases;
        }

        // GET: api/Disease/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDisease([FromRoute] long id)
        {
           

            var disease = await _context.Diseases.SingleOrDefaultAsync(m => m.IdDisease == id);

            if (disease == null)
            {
                return NotFound();
            }

            return Ok(disease);
        }

        // PUT: api/Disease/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDisease([FromRoute] long id, [FromBody] Disease disease)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != disease.IdDisease)
            {
                return BadRequest();
            }

            _context.Entry(disease).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DiseaseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Disease
        [HttpPost]
        public async Task<IActionResult> PostDisease([FromBody] Disease disease)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Diseases.Add(disease);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDisease", new { id = disease.IdDisease }, disease);
        }

        // DELETE: api/Disease/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDisease([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var disease = await _context.Diseases.SingleOrDefaultAsync(m => m.IdDisease == id);
            if (disease == null)
            {
                return NotFound();
            }

            _context.Diseases.Remove(disease);
            await _context.SaveChangesAsync();

            return Ok(disease);
        }

        private bool DiseaseExists(long id)
        {
            return _context.Diseases.Any(e => e.IdDisease == id);
        }
    }
}