﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DentalClinicServer.Models;
using Microsoft.EntityFrameworkCore;

namespace DentalClinicServer.DAL
{
    public class DentalClinicContext:DbContext
    {
        public DentalClinicContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Disease> Diseases { get; set; }
        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<PatientDisease> PatientDiseases { get; set; }
        public DbSet<Treatment> Treatments { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Prescription> Prescriptions { get; set; }
        public DbSet<Personel> Personels { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
    }
}
