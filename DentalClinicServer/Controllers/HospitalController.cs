﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalClinicServer.DAL;
using DentalClinicServer.Models;
using System.Web.Cors;

namespace DentalClinicServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Hospital")]
    public class HospitalController : Controller
    {
        private readonly DentalClinicContext _context;

        public HospitalController(DentalClinicContext context)
        {
            _context = context;
        }

        // GET: api/Hospital
        [HttpGet]
        public IEnumerable<Hospital> GetHospitals()
        {
            return _context.Hospitals;
        }

        // GET: api/Hospital/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetHospital([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var hospital = await _context.Hospitals.SingleOrDefaultAsync(m => m.IdHospital == id);

            if (hospital == null)
            {
                return NotFound();
            }

            return Ok(hospital);
        }

        // PUT: api/Hospital/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHospital([FromRoute] long id, [FromBody] Hospital hospital)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hospital.IdHospital)
            {
                return BadRequest();
            }

            _context.Entry(hospital).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HospitalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Hospital
        [HttpPost]
        public async Task<IActionResult> PostHospital([FromBody] Hospital hospital)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Hospitals.Add(hospital);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetHospital", new { id = hospital.IdHospital }, hospital);
        }

        // DELETE: api/Hospital/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHospital([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var hospital = await _context.Hospitals.SingleOrDefaultAsync(m => m.IdHospital == id);
            var departments = _context.Departments;

            if (hospital == null)
            {
                return NotFound();
            }
            foreach (var depertment in departments)
            {
                if (depertment.Hospital == hospital)
                {
                    _context.Departments.Remove(depertment);
                }
            }
            _context.Hospitals.Remove(hospital);
            await _context.SaveChangesAsync();

            return Ok(hospital);
        }

        private bool HospitalExists(long id)
        {
            return _context.Hospitals.Any(e => e.IdHospital == id);
        }
    }
}