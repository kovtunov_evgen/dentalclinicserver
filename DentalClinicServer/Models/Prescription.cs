﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DentalClinicServer.Models
{
    public class Prescription
    {
        [Key]
        public long IdPrescription { get; set; }

        public virtual Patient Patient { get; set; }
        public virtual Medicine Medicine { get; set; }
    }
}
