﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DentalClinicServer.Models
{
    public class Medicine
    {
        [Key]
        public long IdMedicine { get; set; }
        public string Name { get; set; }
        public int AgeLimit { get; set; }

        public virtual ICollection<Prescription> Prescriptions { get; set; }
        public virtual ICollection<Treatment> Treatments { get; set; }
    }
}
