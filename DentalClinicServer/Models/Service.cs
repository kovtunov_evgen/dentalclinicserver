﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DentalClinicServer.Models
{
    public class Service
    {
        [Key]
        public long IdService { get; set; }

        public virtual Patient Patient { get; set; }
        public virtual Personel Personel { get; set; }
    }
}
