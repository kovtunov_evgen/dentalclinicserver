﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalClinicServer.DAL;
using DentalClinicServer.Models;

namespace DentalClinicServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Treatment")]
    public class TreatmentController : Controller
    {
        private readonly DentalClinicContext _context;

        public class ThreatmentSet{
            List<Medicine> medicine;
            List<Disease> disease;
        }

        public TreatmentController(DentalClinicContext context)
        {
            _context = context;
        }

        // GET: api/Treatment
        [HttpGet]
        public IEnumerable<Treatment> GetTreatments()
        {
            return _context.Treatments;
        }

        // GET: api/Treatment/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTreatment([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var treatment = await _context.Treatments.SingleOrDefaultAsync(m => m.IdTreatment == id);

            if (treatment == null)
            {
                return NotFound();
            }

            return Ok(treatment);
        }

        // PUT: api/Treatment/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTreatment([FromRoute] long id, [FromBody] Treatment treatment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != treatment.IdTreatment)
            {
                return BadRequest();
            }

            _context.Entry(treatment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TreatmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Treatment
        [HttpPost]
        public async Task<IActionResult> PostTreatment([FromBody] Treatment treatment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Treatments.Add(treatment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTreatment", new { id = treatment.IdTreatment }, treatment);
        }

        // DELETE: api/Treatment/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTreatment([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var treatment = await _context.Treatments.SingleOrDefaultAsync(m => m.IdTreatment == id);
            if (treatment == null)
            {
                return NotFound();
            }

            _context.Treatments.Remove(treatment);
            await _context.SaveChangesAsync();

            return Ok(treatment);
        }

        private bool TreatmentExists(long id)
        {
            return _context.Treatments.Any(e => e.IdTreatment == id);
        }
    }
}