﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalClinicServer.DAL;
using DentalClinicServer.Models;

namespace DentalClinicServer.Controllers
{
    [Produces("application/json")]
    [Route("api/PatientDisease")]
    public class PatientDiseaseController : Controller
    {
        private readonly DentalClinicContext _context;

        public PatientDiseaseController(DentalClinicContext context)
        {
            _context = context;
        }

        // GET: api/PatientDisease
        [HttpGet]
        public IEnumerable<PatientDisease> GetPatientDiseases()
        {
            return _context.PatientDiseases;
        }

        // GET: api/PatientDisease/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPatientDisease([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var patientDisease = await _context.PatientDiseases.SingleOrDefaultAsync(m => m.IdPatientDisease == id);

            if (patientDisease == null)
            {
                return NotFound();
            }

            return Ok(patientDisease);
        }

        // PUT: api/PatientDisease/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPatientDisease([FromRoute] long id, [FromBody] PatientDisease patientDisease)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != patientDisease.IdPatientDisease)
            {
                return BadRequest();
            }

            _context.Entry(patientDisease).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PatientDiseaseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PatientDisease
        [HttpPost]
        public async Task<IActionResult> PostPatientDisease([FromBody] PatientDisease patientDisease)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PatientDiseases.Add(patientDisease);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPatientDisease", new { id = patientDisease.IdPatientDisease }, patientDisease);
        }

        // DELETE: api/PatientDisease/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePatientDisease([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var patientDisease = await _context.PatientDiseases.SingleOrDefaultAsync(m => m.IdPatientDisease == id);
            if (patientDisease == null)
            {
                return NotFound();
            }

            _context.PatientDiseases.Remove(patientDisease);
            await _context.SaveChangesAsync();

            return Ok(patientDisease);
        }

        private bool PatientDiseaseExists(long id)
        {
            return _context.PatientDiseases.Any(e => e.IdPatientDisease == id);
        }
    }
}