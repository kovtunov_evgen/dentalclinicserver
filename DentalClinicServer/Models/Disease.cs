﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DentalClinicServer.Models
{
    public class Disease
    {
        [Key]
        public long IdDisease { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<PatientDisease> PatientDiseases { get; set; }
        public virtual ICollection<Treatment> Treatments { get; set; }
    }
}
