﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalClinicServer.DAL;
using DentalClinicServer.Models;

namespace DentalClinicServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Personel")]
    public class PersonelController : Controller
    {
        private readonly DentalClinicContext _context;

        public class PersonelDepartment
        {
            public long IdPersonel { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Post { get; set; }
            public string DepartmentName { get; set; }

            public PersonelDepartment(Personel personel,Department department)
            {
                this.IdPersonel = personel.IdPersonel;
                this.FirstName = personel.FirstName;
                this.LastName = personel.LastName;
                this.Post = personel.Post;
                this.DepartmentName = department.Name;
            }
        }

        public PersonelController(DentalClinicContext context)
        {
            _context = context;
        }

        // GET: api/Personel
        [HttpGet]
        public List<PersonelDepartment> GetPersonels()
        {
            var personels =_context.Personels;
            var departments = _context.Departments;
            List <PersonelDepartment> asd=new List<PersonelDepartment>();
            foreach (var personel in personels)
            {
                foreach (var department in departments)
                {
                    if (personel.DepartmentId == department.IdDepartment)
                    {
                        PersonelDepartment personelDepartment=new PersonelDepartment(personel, department);
                        asd.Add(personelDepartment);
                    }
                }
            }
            return asd;
        }

        // GET: api/Personel/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPersonel([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var personel = await _context.Personels.SingleOrDefaultAsync(m => m.IdPersonel == id);

            if (personel == null)
            {
                return NotFound();
            }

            return Ok(personel);
        }

        // PUT: api/Personel/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPersonel([FromRoute] long id, [FromBody] Personel personel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != personel.IdPersonel)
            {
                return BadRequest();
            }

            _context.Entry(personel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Personel
        [HttpPost]
        public async Task<IActionResult> PostPersonel([FromBody] Personel personel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

           _context.Personels.Add(personel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPersonel", new { id = personel.IdPersonel }, personel);
        }

        // DELETE: api/Personel/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePersonel([FromRoute] long id)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var personel = await _context.Personels.SingleOrDefaultAsync(m => m.IdPersonel == id);
            var services = _context.Services;

            if (personel == null)
            {
                return NotFound();
            }

            foreach (var service in services)
            {
                if (service.Personel == personel) {
                   
                    _context.Services.Remove(service);
                }
            }
            _context.Personels.Remove(personel);
            await _context.SaveChangesAsync();
            return Ok(personel);
        }

        private bool PersonelExists(long id)
        {
            return _context.Personels.Any(e => e.IdPersonel == id);
        }
    }
}