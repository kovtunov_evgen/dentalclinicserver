﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DentalClinicServer.Models
{
    public class Hospital
    {
        [Key]
        public long IdHospital { get; set; }
        public string City { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Department> Departments { get; set; }
    }
}
