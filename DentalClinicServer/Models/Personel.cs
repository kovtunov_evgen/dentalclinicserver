﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DentalClinicServer.Models
{
    public class Personel
    {
        [Key]
        public long IdPersonel { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Post { get; set; }
        public long DepartmentId { get; set; }
        public virtual Department Department { get; set; }
        public virtual ICollection<Service> Services { get; set; }
    }
}
