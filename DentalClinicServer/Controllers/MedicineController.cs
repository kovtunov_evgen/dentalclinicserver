﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalClinicServer.DAL;
using DentalClinicServer.Models;

namespace DentalClinicServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Medicine")]
    public class MedicineController : Controller
    {
        private readonly DentalClinicContext _context;

        public MedicineController(DentalClinicContext context)
        {
            _context = context;
        }

        // GET: api/Medicine
        [HttpGet]
        public IEnumerable<Medicine> GetMedicines()
        {
            return _context.Medicines;
        }

        // GET: api/Medicine/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedicine([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicine = await _context.Medicines.SingleOrDefaultAsync(m => m.IdMedicine == id);

            if (medicine == null)
            {
                return NotFound();
            }

            return Ok(medicine);
        }

        // PUT: api/Medicine/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicine([FromRoute] long id, [FromBody] Medicine medicine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicine.IdMedicine)
            {
                return BadRequest();
            }

            _context.Entry(medicine).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medicine
        [HttpPost]
        public async Task<IActionResult> PostMedicine([FromBody] Medicine medicine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Medicines.Add(medicine);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedicine", new { id = medicine.IdMedicine }, medicine);
        }

        // DELETE: api/Medicine/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicine([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicine = await _context.Medicines.SingleOrDefaultAsync(m => m.IdMedicine == id);
            if (medicine == null)
            {
                return NotFound();
            }

            _context.Medicines.Remove(medicine);
            await _context.SaveChangesAsync();

            return Ok(medicine);
        }

        private bool MedicineExists(long id)
        {
            return _context.Medicines.Any(e => e.IdMedicine == id);
        }
    }
}