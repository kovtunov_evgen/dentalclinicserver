﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DentalClinicServer.Models
{
    public class Treatment
    {
        [Key]
        public long IdTreatment { get; set; }

        public virtual Medicine Medicine { get; set; }
        public virtual Disease Disease { get; set; }
    }
}
