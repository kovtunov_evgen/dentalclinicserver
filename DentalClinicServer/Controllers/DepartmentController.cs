﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalClinicServer.DAL;
using DentalClinicServer.Models;

namespace DentalClinicServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Department")]
    public class DepartmentController : Controller
    {
        private readonly DentalClinicContext _context;

        public DepartmentController(DentalClinicContext context)
        {
            _context = context;
        }

        // GET: api/Department
        [HttpGet]
        public IEnumerable<Department> GetDepartments()
        {
            return _context.Departments;
        }

        // GET: api/Department/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDepartment([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var department = await _context.Departments.SingleOrDefaultAsync(m => m.IdDepartment == id);

            if (department == null)
            {
                return NotFound();
            }

            return Ok(department);
        }

        // PUT: api/Department/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDepartment([FromRoute] long id, [FromBody] Department department)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != department.IdDepartment)
            {
                return BadRequest();
            }

            _context.Entry(department).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DepartmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Department
        [HttpPost]
        public async Task<IActionResult> PostDepartment([FromBody] Department department)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Departments.Add(department);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDepartment", new { id = department.IdDepartment }, department);
        }

        // DELETE: api/Department/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDepartment([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var department = await _context.Departments.SingleOrDefaultAsync(m => m.IdDepartment == id);
            var patients = _context.Patients;
            var personels = _context.Personels;
            if (department == null)
            {
                return NotFound();
            }
            foreach(var patient in patients)
            {
                if (patient.Department == department)
                    _context.Patients.Remove(patient);
            }
            foreach (var personel in personels)
            {
                if (personel.Department == department)
                    _context.Personels.Remove(personel);
            }
            _context.Departments.Remove(department);
            await _context.SaveChangesAsync();

            return Ok(department);
        }

        private bool DepartmentExists(long id)
        {
            return _context.Departments.Any(e => e.IdDepartment == id);
        }
    }
}